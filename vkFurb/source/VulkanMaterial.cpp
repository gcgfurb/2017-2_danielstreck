#include "VulkanMaterial.h"
#include "VulkanTexture.h"

VulkanMaterial::VulkanMaterial() : vulkanTexture(new VulkanTexture)
{
}

VulkanMaterial::~VulkanMaterial()
{
	vulkanShader = nullptr;
	delete vulkanShader;
}


void VulkanMaterial::setTexture(VulkanTexture *texture)
{
	vulkanTexture = texture;
}

void VulkanMaterial::setShader(VulkanShader *shader)
{
	this->vulkanShader = shader;
}

const VkDescriptorSet * VulkanMaterial::getDescriptorSet()
{
	return vulkanShader->getDescriptorSet(); 
}