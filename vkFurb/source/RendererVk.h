#pragma once

#include "VulkanHeader.h"
#include "../../commonBase/source/Renderer.h"


#define VKRENDERER RendererVk::getInstance()

//----- init ---------
//1. init vulkan instance
// create surface
//2. get physical device
//3. get logical device
//4. get logical device queues for command submission
//----- presentation ---------
//5. create swapchain to present 


// centralizar infos sobre devices
struct VulkanDevice
{
	VkPhysicalDevice vkPhysicalDevice;
	uint32_t GraphicsQueueFamilyIndex; // selecionada do vkPhysicalDevice = VK_GRAPHICS_BIT
	uint32_t PresentQueueFamilyIndex;
	VkDevice vkDevice;

	VkQueue graphicsQueue;
	VkQueue presentQueue;
};


struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

const std::vector<const char*> deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};


static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData) {
	std::cerr << "validation layer: " << msg << std::endl;

	return VK_FALSE;
}

// macro para encurtar o downcast
#define PtrDownCast static_cast<VulkanDrawableObj*>

class RendererVk : public Renderer
{
public:
	//***SINGLETON****
	static RendererVk* getInstance()
	{
		static RendererVk instance; // Guaranteed to be destroyed.
								    // Instantiated on first use.
		return &instance;
	}

	// C++ 11
	// =======
	// We can use the better technique of deleting the methods
	// we don't want.
	RendererVk(RendererVk const&) = delete;
	void operator=(RendererVk const&) = delete;
	//***SINGLETON****
	RendererVk() {};
	~RendererVk();

	GLFWwindow * getWindow() { return _glfwWindow; };

	void initGLFW();
	void prepare();

	void mainLoop();

	void drawFrame();
	void update(float dt);

	void preDraw();

	static void onWindowResized(GLFWwindow *window, int width, int height) 
	{
		if (width == 0 || height == 0) return;

		VKRENDERER->getInstance()->recreateSwapChain();
	}

	std::vector<const char*> getRequiredExtensions();

	void setupDebugCallback();

	bool isDeviceSuitable(VkPhysicalDevice device);
	bool checkDeviceExtensionSupport(VkPhysicalDevice device);
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR & capabilities);

	void initVulkan();
	bool getPhysicalDevice();
	bool getLogicalDevice();
	bool createSemaphores();
	bool createSwapChain();
	void recreateSwapChain();
	void cleanupSwapChain();
	void createSwapChainImageViews();
	void createRenderPass();
	void createGraphicsPipeline(std::string vertShader, std::string fragShader, VkPipeline & outPipeline, VkDescriptorSetLayout descriptorSetLayout);
	void createFramebuffers();
	void createCommandPool();
	void createDepthResources();
	void recordDrawCommandBuffers();
	void createDrawCommandBuffers();

	void createTextureImage(VulkanTexture & vulkanTexture, std::string texturePath);
	void createTextureImageView(VulkanTexture & vulkanTexture);
	void createTextureSampler(VulkanTexture & vulkanTexture);


public:


	VkInstance _vkInstance;
	VkDevice vkDevice;
	VkPhysicalDevice vkPhysicalDevice;

	VulkanSwapChain _swapChain;

	VkQueue graphicsQueue;
	VkQueue presentQueue;

	VkCommandPool commandPool;

	std::chrono::time_point<std::chrono::steady_clock> startTime;

private:
	bool enableValidationLayers;

	VkDebugReportCallbackEXT callback;

	GLFWwindow *_glfwWindow;

	uint32_t GraphicsQueueFamilyIndex; // selecionada do vkPhysicalDevice = VK_GRAPHICS_BIT
	uint32_t PresentQueueFamilyIndex;

	VkSurfaceKHR _vkSurfaceKHR;

	VkSemaphore _renderingFinishedSemaphore;
	VkSemaphore _imageAvailableSemaphore;

	VkPipelineLayout pipelineLayout;
	VkRenderPass renderPass;

	std::vector<VkCommandBuffer> commandBuffers;

	VkImage depthImage;
	VkDeviceMemory depthImageMemory;
	VkImageView depthImageView;


};
