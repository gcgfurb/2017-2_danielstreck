#pragma once

#include "VulkanHeader.h"

class VulkanMaterial
{
public:
	VulkanMaterial();
	~VulkanMaterial();

	void setTexture(VulkanTexture *texture);

	void setShader(VulkanShader *shader);
	VulkanShader* getShader() { return vulkanShader; }

	const VkDescriptorSet *getDescriptorSet();

	VulkanTexture* vulkanTexture;


private:

	VulkanShader *vulkanShader;
};

