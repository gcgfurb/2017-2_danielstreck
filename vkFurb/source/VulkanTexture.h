#pragma once
#include "VulkanHeader.h"

class VulkanTexture
{
public:
	VulkanTexture();
	VulkanTexture(const std::string &path);
	//VulkanTexture(const VulkanTexture& other) {};
	~VulkanTexture();

	void createTexture(const std::string &path);

	VkImage textureImage;
	VkDeviceMemory textureImageMemory;
	VkImageView textureImageView;
	VkSampler textureSampler;
};

