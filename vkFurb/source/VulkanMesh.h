#pragma once

#include "VulkanHeader.h"

class VulkanMesh : public Mesh
{

public:
	VulkanMesh(std::string path);
	~VulkanMesh();

	void loadModel(std::string path);

	void setModel(std::vector<Vertex>, std::vector<uint32_t> indices);

	const VkBuffer& getIndexBuffer() { return indexBuffer; }
	VkBuffer getVertexBuffer() { return vertexBuffer; }
	VkDeviceMemory getIndexBufferMemory() { return indexBufferMemory; }
	VkDeviceMemory getVertexBufferMemory() { return vertexBufferMemory; }
	
	const std::vector<uint32_t> getIndices() { return indices; }

	// cache tamanho dos indices para nao precisar computar size todo frame
	size_t indicesSize;

private:

	void createVertexBuffer();
	void createIndexBuffer();

private:

	std::vector<Vertex> vertices;
	std::vector<uint32_t> indices;

	VkBuffer vertexBuffer;
	VkDeviceMemory vertexBufferMemory;

	VkBuffer indexBuffer;
	VkDeviceMemory indexBufferMemory;
};

