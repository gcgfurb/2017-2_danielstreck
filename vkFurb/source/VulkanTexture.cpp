#include "VulkanTexture.h"



VulkanTexture::VulkanTexture()
{
}

VulkanTexture::VulkanTexture(const std::string &path)
{
	createTexture(path);
}


VulkanTexture::~VulkanTexture()
{

	vkDestroySampler(RendererVk::getInstance()->vkDevice, textureSampler, nullptr);
	vkDestroyImageView(RendererVk::getInstance()->vkDevice, textureImageView, nullptr);
	vkDestroyImage(RendererVk::getInstance()->vkDevice, textureImage, nullptr);
	vkFreeMemory(RendererVk::getInstance()->vkDevice, textureImageMemory, nullptr);
}


void VulkanTexture::createTexture(const std::string &path)
{
	VKRENDERER->createTextureImage(*this, path);
	VKRENDERER->createTextureImageView(*this);
	VKRENDERER->createTextureSampler(*this);
}
