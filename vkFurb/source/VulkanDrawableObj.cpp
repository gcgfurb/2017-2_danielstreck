#include "VulkanDrawableObj.h"

#include "VulkanMesh.h"
#include "VulkanMaterial.h"

#include <glm\trigonometric.hpp>

VulkanDrawableObj::VulkanDrawableObj() 
{
	transform = glm::mat4(1.f);

	createUniformBuffer();
}

VulkanDrawableObj::~VulkanDrawableObj()
{
	delete material;
	delete mesh;

	vkDestroyBuffer(RendererVk::getInstance()->vkDevice, uniformBuffer, nullptr);
	vkFreeMemory(RendererVk::getInstance()->vkDevice, uniformBufferMemory, nullptr);
}

void VulkanDrawableObj::loadModel(std::string path)
{
	//if (mesh == nullptr)
	//{
	//	// todo: dont create mesh everytime
	//	mesh = (new VulkanMesh);
	//}
	static_cast<VulkanMesh*>(mesh)->loadModel(path);
}

void VulkanDrawableObj::update(float dt)
{
	updateUniformBuffer();
}

void VulkanDrawableObj::createUniformBuffer()
{
	VkDeviceSize bufferSize = sizeof(UniformBufferObject);
	VulkanHelper::createBuffer(VKRENDERER->vkPhysicalDevice, VKRENDERER->vkDevice, bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffer, uniformBufferMemory);
}

void VulkanDrawableObj::updateUniformBuffer()
{

	static auto startTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	float time = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count() / 1000.0f;

	transform = glm::mat4(1.f);
	transform *= glm::translate(glm::mat4(1.f), position);
	transform *= glm::scale(glm::mat4(), scale);

	transform *= glm::rotate(glm::mat4(1.f), time * glm::radians(90.0f), glm::vec3(0, 1.0f, 0.0f));

	ubo.lightPos = VKRENDERER->scene->light.lightPos;
	ubo.model = transform;
	ubo.view = VKRENDERER->scene->camera->view;
	ubo.proj = VKRENDERER->scene->camera->projection;
	ubo.proj[1][1] *= -1; // inverter porque no opengl y eh o contrario



#ifdef VKBUFFERS
	void* data;
	vkMapMemory(VKRENDERER->vkDevice, uniformBufferMemory, 0, sizeof(ubo), 0, &data);
	memcpy(data, &ubo, sizeof(ubo));
	vkUnmapMemory(VKRENDERER->vkDevice, uniformBufferMemory);
#endif
}