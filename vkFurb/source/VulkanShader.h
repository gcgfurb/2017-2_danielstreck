#pragma once
#include "VulkanHeader.h"

class VulkanShader
{
public:

	VulkanShader(std::string vertPath, std::string fragPath);
	~VulkanShader();


	void prepare();

	void createDescriptorPool();
	void createDescriptorSet();
	void updateDescriptorSet(VkBuffer uniformBuffer, VulkanTexture *vulkanTexture);
	void createDescriptorSetLayout();
	void recreateGraphicsPipeline();

	VkDescriptorPool getDescriptorPool() { return descriptorPool; }
	const VkDescriptorSet* getDescriptorSet() { return &descriptorSet; }

	VkDescriptorSet descriptorSet;
	VkDescriptorPool descriptorPool;
	VkDescriptorSetLayout descriptorSetLayout;

	VkPipeline graphicsPipeline;

	std::string vertPath;
	std::string fragPath;
};