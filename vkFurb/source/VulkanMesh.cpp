#include "VulkanMesh.h"


VulkanMesh::VulkanMesh(std::string path)
{
	loadModel(path);
}

VulkanMesh::~VulkanMesh()
{
	vkDestroyBuffer(RendererVk::getInstance()->vkDevice, indexBuffer, nullptr);
	vkFreeMemory(RendererVk::getInstance()->vkDevice, indexBufferMemory, nullptr);

	vkDestroyBuffer(RendererVk::getInstance()->vkDevice, vertexBuffer, nullptr);
	vkFreeMemory(RendererVk::getInstance()->vkDevice, vertexBufferMemory, nullptr);
}

void VulkanMesh::loadModel(std::string path)
{
	VulkanHelper::loadModel(path, vertices, indices);

	indicesSize = indices.size();

	createVertexBuffer();
	createIndexBuffer();
}

void VulkanMesh::setModel(std::vector<Vertex> vertices, std::vector<uint32_t> indices)
{
	this->vertices = vertices;
	this->indices = indices;

	createVertexBuffer();
	createIndexBuffer();
}


void VulkanMesh::createVertexBuffer()
{
	VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

	// criar staging buffer para mapear na memoria RAM e so depois passar para VRAM
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	VulkanHelper::createBuffer(VKRENDERER->vkPhysicalDevice, VKRENDERER->vkDevice, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

	void* data;
	vkMapMemory(VKRENDERER->vkDevice, stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, vertices.data(), (size_t)bufferSize);
	vkUnmapMemory(VKRENDERER->vkDevice, stagingBufferMemory);

	VulkanHelper::createBuffer(VKRENDERER->vkPhysicalDevice, VKRENDERER->vkDevice, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);

	VulkanHelper::copyBuffer(VKRENDERER->vkDevice, VKRENDERER->commandPool, VKRENDERER->graphicsQueue, stagingBuffer, vertexBuffer, bufferSize);

	vkDestroyBuffer(VKRENDERER->vkDevice, stagingBuffer, nullptr);
	vkFreeMemory(VKRENDERER->vkDevice, stagingBufferMemory, nullptr);
}

void VulkanMesh::createIndexBuffer()
{
	VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	VulkanHelper::createBuffer(VKRENDERER->vkPhysicalDevice, VKRENDERER->vkDevice, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

	void* data;
	vkMapMemory(VKRENDERER->vkDevice, stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, indices.data(), (size_t)bufferSize);
	vkUnmapMemory(VKRENDERER->vkDevice, stagingBufferMemory);

	VulkanHelper::createBuffer(VKRENDERER->vkPhysicalDevice, VKRENDERER->vkDevice, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory);

	VulkanHelper::copyBuffer(VKRENDERER->vkDevice, VKRENDERER->commandPool, VKRENDERER->graphicsQueue, stagingBuffer, indexBuffer, bufferSize);

	vkDestroyBuffer(VKRENDERER->vkDevice, stagingBuffer, nullptr);
	vkFreeMemory(VKRENDERER->vkDevice, stagingBufferMemory, nullptr);
	
}
