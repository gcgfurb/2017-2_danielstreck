#pragma once
#define VK_USE_PLATFORM_WIN32_KHR

#include "VulkanHeader.h"

const std::vector<const char*> validationLayers = {
	"VK_LAYER_LUNARG_standard_validation"
};

struct QueueFamilyIndices {
	int graphicsFamily = -1;
	int presentFamily = -1;

	bool isComplete() {
		return graphicsFamily >= 0 && presentFamily >= 0;
	}
};

class VulkanHelper
{
public:

	static QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR vkSurfaceKHR);

	static void createBuffer(VkPhysicalDevice & physicalDevice, const VkDevice & device, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer & buffer, VkDeviceMemory & bufferMemory);

	static uint32_t findMemoryType(VkPhysicalDevice & physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties);
	
	static VkFormat findDepthFormat(const VkPhysicalDevice & physicalDevice);

	static VkFormat findSupportedFormat(const VkPhysicalDevice & physicalDevice, const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

	static bool hasStencilComponent(VkFormat format);

	static void copyBuffer(const VkDevice & device, const VkCommandPool & commandPool, const VkQueue & graphicsQueue, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);

	static VkShaderModule createShaderModule(const std::vector<char>& code, VkDevice vkDevice);

	static bool createPresentationSurface(VkInstance vkInstance, GLFWwindow * glfwWindow, VkSurfaceKHR * outSurfaceKHR);

	static void createImage(const VkDevice & device, VkPhysicalDevice & physicalDevice, uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage & image, VkDeviceMemory & imageMemory);

	static VkImageView createImageView(const VkDevice & device, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);

	static void copyBufferToImage(const VkDevice & device, const VkQueue & graphicsQueue, const VkCommandPool & commandPool, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);

	static void transitionImageLayout(const VkDevice & device, const VkQueue & graphicsQueue, const VkCommandPool & commandPool, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);

	static VkSampler createTextureSampler(const VkDevice & device);

	static VkCommandBuffer beginSingleTimeCommands(const VkDevice & device, const VkCommandPool & commandPool);

	static void endSingleTimeCommands(const VkDevice & device, const VkQueue & graphicsQueue, VkCommandBuffer commandBuffer, const VkCommandPool & commandPool);

	static void loadModel(std::string MODEL_PATH, std::vector<Vertex>& outVertices, std::vector<uint32_t>& outIndices);

	static std::vector<char> readFile(const std::string & filename);

	static VkResult CreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT * pCreateInfo, const VkAllocationCallbacks * pAllocator, VkDebugReportCallbackEXT * pCallback);

	static bool checkValidationLayerSupport();

};