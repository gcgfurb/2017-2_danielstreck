#pragma once

#include "VulkanHeader.h"


class VulkanDrawableObj : public DrawableObj
{
public:
	VulkanMaterial* material;

	VkBuffer uniformBuffer;
	VkDeviceMemory uniformBufferMemory;
	UniformBufferObject ubo; // transformacoes

public:
	VulkanDrawableObj();
	~VulkanDrawableObj();

	void loadModel(std::string path);
	void update(float dt) override;

	VulkanMaterial* getMaterial() { return material; }
	VulkanMesh* getMesh() { return reinterpret_cast<VulkanMesh*>(mesh); }

	void setMesh(VulkanMesh *mesh) { this->mesh = reinterpret_cast<Mesh*>(mesh); };

private:
	void createUniformBuffer();
	void updateUniformBuffer();

};

