#pragma once

#include "../VulkanHeader.h"

class Cena3 : public Scene
{
public:
	Cena3()
	{
		name = "Cena3";

		VulkanTexture *furTexture = (new VulkanTexture("../textures/fur.jpg"));
		VulkanTexture *redTexture = (new VulkanTexture("../textures/red.png"));
		VulkanTexture *buddhaTexture = (new VulkanTexture("../textures/teapot.jpg"));

		VulkanMaterial* material(new VulkanMaterial);
		VulkanMaterial* redMaterial(new VulkanMaterial);
		VulkanMaterial* buddhMaterial(new VulkanMaterial);

		VulkanShader* shader(new VulkanShader("../shaders/vert.spv", "../shaders/frag.spv"));
		VulkanShader* redShader(new VulkanShader("../shaders/vert.spv", "../shaders/frag.spv"));
		VulkanShader* buddhaShader(new VulkanShader("../shaders/vert.spv", "../shaders/frag.spv"));

		VulkanMesh* bunnyMesh(new VulkanMesh("../models/bunny.obj"));
		VulkanMesh* monkeyMesh(new VulkanMesh("../models/monkey.obj"));
		VulkanMesh* buddhaMesh(new VulkanMesh("../models/buddha.obj"));

		redMaterial->setShader(redShader);
		material->setShader(shader);
		buddhMaterial->setShader(buddhaShader);

		float row = 10;
		// 120 objs
		for (int j = 0; j < 4; j++)
		{
			for (int i = 0; i < row; i++)
			{
				DrawableObj* obj1(new VulkanDrawableObj);

				PtrDownCast(obj1)->material = material;
				PtrDownCast(obj1)->getMaterial()->setTexture(furTexture);

				shader->updateDescriptorSet(PtrDownCast(obj1)->uniformBuffer, furTexture);

				PtrDownCast(obj1)->setMesh(bunnyMesh);

				PtrDownCast(obj1)->setPosition(glm::vec3((i - row / 2) * 3, 1, -(j * 3)));
				PtrDownCast(obj1)->setScale(glm::vec3(1));

				this->AddObject(obj1);
			}

			for (float i = 0; i < row; i++)
			{
				DrawableObj* obj1(new VulkanDrawableObj);

				PtrDownCast(obj1)->material = redMaterial;
				PtrDownCast(obj1)->getMaterial()->setTexture(redTexture);

				redShader->updateDescriptorSet(PtrDownCast(obj1)->uniformBuffer, redTexture);

				PtrDownCast(obj1)->setMesh(monkeyMesh);

				PtrDownCast(obj1)->setPosition(glm::vec3((i - row / 2) * 3, 1, -((j +3) * 6)));
				PtrDownCast(obj1)->setScale(glm::vec3(1));

				this->AddObject(obj1);
			}

			for (float i = 0; i < row; i++)
			{
				DrawableObj* obj1(new VulkanDrawableObj);

				PtrDownCast(obj1)->material = buddhMaterial;
				PtrDownCast(obj1)->getMaterial()->setTexture(buddhaTexture);

				buddhaShader->updateDescriptorSet(PtrDownCast(obj1)->uniformBuffer, buddhaTexture);

				PtrDownCast(obj1)->setMesh(buddhaMesh);

				PtrDownCast(obj1)->setPosition(glm::vec3((i - row / 2) * 3, 1, 2));
				PtrDownCast(obj1)->setScale(glm::vec3(2.5));

				this->AddObject(obj1);
			}
		}


		std::cout << getSceneGraph().size() << std::endl;

		Camera *camera = new Camera;
		camera->view = glm::lookAt(glm::vec3(-1, 8, 12.0f), glm::vec3(-1.0f, 1.0f,-4), glm::vec3(0.0f, 1.0f, 0.0f));
		camera->projection = glm::perspective(glm::radians(60.0f), VKRENDERER->_swapChain.getAspectRatio() , 0.1f, 100.0f);

		Light light;
		light.lightPos = glm::vec4(row / 2, 2, row / 2, 1);

		this->light = light;
		this->camera = camera;
	}
};