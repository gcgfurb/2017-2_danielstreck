#pragma once

#include "../VulkanHeader.h"

class Cena1 : public Scene
{
public:
	Cena1()
	{
		name = "Cena1";
		// cria��o dos assets que ser�o utilizdados
		VulkanTexture *texture(new VulkanTexture("../textures/bronze.jpg"));
		VulkanMaterial* material(new VulkanMaterial);
		VulkanShader* shader(new VulkanShader("../shaders/vert.spv", "../shaders/frag.spv"));
		VulkanMesh* mesh(new VulkanMesh("../models/buddha.obj"));
		// atribuir o shader ao material
		material->setShader(shader);
		// cria��o do objeto gr�fico
		DrawableObj* obj1(new VulkanDrawableObj);
		// configura��o do objeto gr�ifco
		PtrDownCast(obj1)->material = material;
		PtrDownCast(obj1)->getMaterial()->setTexture(texture);
		shader->updateDescriptorSet(PtrDownCast(obj1)->uniformBuffer, texture);
		PtrDownCast(obj1)->setMesh(mesh);
		PtrDownCast(obj1)->setPosition(glm::vec3(0));
		PtrDownCast(obj1)->setScale(glm::vec3(1));
		// adicionar o objeto gr�fico � cena
		this->AddObject(obj1);
		// configurar luz e camera da cena
		this->light = Light(glm::vec4(0, 5, 10, 0));
		this->camera = new Camera(glm::vec3(0, 0.5f, 1), glm::vec3(0.0f, .1f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	}
};