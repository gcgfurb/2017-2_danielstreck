
#include "../VulkanHeader.h"

class Cena2 : public Scene
{
public:
	Cena2()
	{
		name = "Cena2";

		VulkanTexture *texture = (new VulkanTexture("../textures/fur.jpg"));

		VulkanMaterial* material(new VulkanMaterial);

		VulkanShader* shader(new VulkanShader("../shaders/vert.spv", "../shaders/frag.spv"));

		VulkanMesh* mesh(new VulkanMesh("../models/bunny.obj"));

		//shader->prepare();

		material->setShader(shader);

		float row = 150;
		// 289 objs
		for (float i = 0; i < row; i += 3)
		{
			for (float j = 0; j < row; j += 3)
			{
				DrawableObj* obj1(new VulkanDrawableObj);

				PtrDownCast(obj1)->material = material;
				PtrDownCast(obj1)->getMaterial()->setTexture(texture);

				shader->updateDescriptorSet(PtrDownCast(obj1)->uniformBuffer, texture);

				PtrDownCast(obj1)->setMesh(mesh);

				PtrDownCast(obj1)->setPosition(glm::vec3(i - row / 2, 1, j - row / 2));
				PtrDownCast(obj1)->setScale(glm::vec3(1));

				this->AddObject(obj1);
			}
		}

		std::cout << getSceneGraph().size() << std::endl;

		Camera *camera = new Camera;
		camera->view = glm::lookAt(glm::vec3(0, 20, 40.0f), glm::vec3(0.0f, 1.0f, 0), glm::vec3(0.0f, 1.0f, 0.0f));
		camera->projection = glm::perspective(glm::radians(60.0f), VKRENDERER->_swapChain.getAspectRatio(), 0.1f, 100.0f);

		Light light;
		light.lightPos = glm::vec4(row / 2, 2, row / 2, 1);

		this->light = light;
		this->camera = camera;
	}
};