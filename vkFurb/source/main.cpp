
//#include "VulkanHeader.h"

#include "cenas\Cena1.h"
#include "cenas\Cena2.h"
#include "cenas\Cena3.h"

#include <fstream>
#include <iostream>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <sstream>

#include "../../commonBase/source/Utils/Diagnostics.h"
#include "../../commonBase/source/Utils/CpuUsage.h"

int main()
{
	VKRENDERER->prepare();

	//std::shared_ptr<Scene> scene(new Cena1());
	std::shared_ptr<Scene> scene(new Cena2());
	//std::shared_ptr<Scene> scene(new Cena3());

	VKRENDERER->scene = scene;

	VKRENDERER->preDraw();
	
	const float UPDATE_STEP = 1.f / 60.f;
	double total_time = 0;
	double current_time = glfwGetTime();
	double start_time = current_time;

	unsigned int frameCounter = 0;
	float fpsTimer, lastFPS = 0;
	double frameTime = 0;
	float frameTimer = 0;

	float frameTimeAccumulator = 0;
	float fpsAccumulator = 0, ramAccumulator = 0, cpuAccumulator = 0;
	float ramThisFrame = 0;
	short cpuThisFrame = 0;

	std::string stats;
	CpuUsage cpuUsage;

	while (!glfwWindowShouldClose(VKRENDERER->getWindow()))
	{
		glfwPollEvents();

		auto tStart = std::chrono::high_resolution_clock::now();

		double new_time = glfwGetTime();
		double frame_time = new_time - current_time;
		current_time = new_time;
		total_time += frame_time;

		while (total_time >= UPDATE_STEP)
		{
			VKRENDERER->update(UPDATE_STEP);

			total_time -= UPDATE_STEP;
		}

		VKRENDERER->drawFrame();

		auto tEnd = std::chrono::high_resolution_clock::now();
		auto tDiff = std::chrono::duration<double, std::milli>(tEnd - tStart).count();
		frameTimer = (float)tDiff / 1000.0f;

		fpsAccumulator += static_cast<uint32_t>(1.0f / frameTimer);
		frameTimeAccumulator += frameTimer;

		ramThisFrame = Diagnostics::GetWorkingSizeMemory();
		cpuThisFrame = cpuUsage.GetUsage();

		ramAccumulator += ramThisFrame;
		cpuAccumulator += cpuThisFrame;

		fpsTimer += (float)tDiff;
		if (fpsTimer > 100.0f)
		{
			lastFPS = static_cast<uint32_t>(1.0f / frameTimer);
			fpsTimer = 0.0f;

			stats = "TCC Daniel Streck - Vulkan  |  FPS: " + std::to_string(lastFPS) +
				"  |  frame time(ms): " + std::to_string(frameTimer * 1000) +
				" | CPU: " + std::to_string(cpuThisFrame) +
				"%  |  RAM: " + std::to_string(ramThisFrame) + " MBs";

			glfwSetWindowTitle(VKRENDERER->getWindow(),  stats.c_str());
		}

		frameCounter++;

		if (glfwGetTime() - start_time >= 20)
			break;
	}


	float avgFps = fpsAccumulator / frameCounter;
	float avgFrameTime = (frameTimeAccumulator / frameCounter) * 1000;
	float avgRAMUsage = ramAccumulator / frameCounter;
	float avgCPUUsage = cpuAccumulator / frameCounter;

	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	std::ostringstream oss;
	oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
	auto str = oss.str();

	std::ofstream logFile;
	logFile.open("log-vulkan " + scene->name + " " + str + ".txt");
	logFile << "avg fps: " + std::to_string(avgFps) << std::endl;
	logFile << "avg frame time: " + std::to_string(avgFrameTime) + "(ms)" << std::endl;
	logFile << "avg CPU usage: " + std::to_string(avgCPUUsage) + "(%)" << std::endl;
	logFile << "avg RAM usage: " + std::to_string(avgRAMUsage) + "(MB)" << std::endl;
	logFile << "frame count: " + std::to_string(frameCounter);
	logFile.close();

	return 0;
}