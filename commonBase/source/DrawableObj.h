#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <string>
#include <memory>

#include "Mesh.h"

class DrawableObj
{
public:

	//virtual void loadModel(std::string path);
	virtual void update(float dt) = 0;

	void setPosition(glm::vec3 position) { this->position = position; /*transform *= position; */}
	void setScale(glm::vec3 scale) { this->scale = scale; }

	glm::vec3 position;
	glm::vec3 scale;

	glm::mat4 transform;

	Mesh* mesh;
};

