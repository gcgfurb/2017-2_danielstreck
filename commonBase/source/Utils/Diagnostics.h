#pragma once

#include <windows.h>
#include <psapi.h>

#pragma comment(lib, "psapi.lib") //remember added it

namespace Diagnostics 
{
	static float GetWorkingSizeMemory()
	{
		PROCESS_MEMORY_COUNTERS memCounter;
		BOOL result = GetProcessMemoryInfo(GetCurrentProcess(),
			&memCounter,
			sizeof(memCounter));

		return memCounter.WorkingSetSize / 1024.f / 1024.f;
	}
}