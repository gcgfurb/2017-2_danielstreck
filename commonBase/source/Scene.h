#pragma once

#include <memory>
#include <list>

#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm\trigonometric.hpp>

struct Light
{
	Light(glm::vec4 &pos)
	{
		lightPos = pos;
	}

	Light() {};

	glm::vec4 lightPos = glm::vec4(0);
};

struct Camera
{
	Camera(glm::vec3 &eye, glm::vec3 &center, glm::vec3 &up)
	{
		projection = glm::perspective(glm::radians(60.0f), (float)16 / (float)9, 0.1f, 100.0f);
		view = glm::lookAt(eye, center, up);
	}

	Camera()
	{
		projection = glm::perspective(glm::radians(60.0f), (float)16 / (float)9, 0.1f, 100.0f);
		view = glm::lookAt(glm::vec3(0, 0, 10), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	}

	glm::mat4 view;
	glm::mat4 projection;

	glm::vec3 camPos = glm::vec3(0, 0, 10);

	void move(glm::vec3 translation) 
	{
		view = glm::lookAt(camPos += translation, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	}
};


class DrawableObj;

class Scene
{
public:
	void AddObject(DrawableObj *obj) { sceneGraph.push_back(obj); }


public:

	std::list<DrawableObj*>& getSceneGraph() { return sceneGraph; }

	Light light;
	Camera *camera;

	std::string name;

private:

	std::list<DrawableObj*> sceneGraph;
};

