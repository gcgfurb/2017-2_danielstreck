#pragma once

#include <memory>

class GLFWwindow;
class Scene;

class Renderer
{
public:
	GLFWwindow * getWindow() { return _glfwWindow; };

	virtual void initGLFW() = 0;
	virtual void prepare() = 0;
	virtual void drawFrame() = 0;
	virtual void mainLoop() = 0;
	virtual void update(float dt) = 0;


public:
	const int WIDTH = 1280, HEIGHT = 720;

	GLFWwindow *_glfwWindow;

	std::shared_ptr<Scene> scene;
};