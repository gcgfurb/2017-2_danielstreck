#version 150


layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) in vec3 fragColor;
layout (location = 1) in vec3 inViewVec;
layout (location = 2) in vec3 inLightVec;
layout(location = 3) in vec2 fragTexCoord;
layout (location = 4) in vec3 inNormal;


layout(location = 0) out vec4 outFragColor;

uniform mat4 model;
uniform vec3 cameraPosition;

// material settings
uniform sampler2D materialTex;
uniform float materialShininess;
uniform vec3 materialSpecularColor;

uniform struct Light {
   vec3 position;
   vec3 intensities; //a.k.a the color of the light
   float attenuation;
   float ambientCoefficient;
} light;



in vec3 fragVert;

out vec4 finalColor;

void main() {

   vec3 intensities = vec3(1,1,1); //a.k.a the color of the light
   float attenuation = 0.5;
   float ambientCoefficient = 0.5;

    vec3 normal = inNormal;
    vec3 surfacePos = inViewVec;
    vec4 surfaceColor = texture(materialTex, fragTexCoord);
    vec3 surfaceToLight = inNormal;
    vec3 surfaceToCamera = normalize(cameraPosition - surfacePos);
    
    //ambient
    vec3 ambient = ambientCoefficient * surfaceColor.rgb * intensities;

    //diffuse
    float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));
    vec3 diffuse = diffuseCoefficient * surfaceColor.rgb * light.intensities;
    
    //specular
    float specularCoefficient = 0.0;
    if(diffuseCoefficient > 0.0)
        specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), materialShininess);
    vec3 specular = specularCoefficient * materialSpecularColor * light.intensities;
    
    //attenuation
    float distanceToLight = length(light.position - surfacePos);
    float attenuation = 1.0 / (1.0 + light.attenuation * pow(distanceToLight, 2));

    //linear color (color before gamma correction)
    vec3 linearColor = ambient + attenuation*(diffuse + specular);
    
    //final color (after gamma correction)
    vec3 gamma = vec3(1.0/2.2);
    outFragColor = vec4(pow(linearColor, gamma), surfaceColor.a);
}