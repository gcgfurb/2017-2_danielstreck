#version 450 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inColor;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 normal;


layout(location = 0)out vec2 outTexCoords;
layout(location = 1)out vec3 outViewVec;

layout(location = 2)out vec3 outLightVec;
layout(location = 3)out vec3 outNormal;
layout(location = 4)out vec3 outfragColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec4 lightPos;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);
    outfragColor = inColor;
    outTexCoords = texCoords;

	outNormal = normalize(mat3(transpose(inverse(model))) * normal);

	vec4 pos = model * vec4(position, 1.0);
	vec3 lPos = lightPos.xyz;
	outLightVec = lPos - pos.xyz;

	vec3 camPos = vec3(view[0][3], view[1][3], view[2][3]);	

	outViewVec = pos.xyz;	
}