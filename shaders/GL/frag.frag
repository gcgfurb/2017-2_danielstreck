#version 450 core
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(location = 0) in vec2 inTexCoords;
layout(location = 1) in vec3 inViewVec;
layout(location = 2) in vec3 inLightVec;
layout(location = 3) in vec3 inNormal;
layout(location = 4) in vec3 infragColor;

out vec4 color;

uniform sampler2D texture_diffuse1;

void main()
{    
    vec4 tmpColor = texture(texture_diffuse1, inTexCoords);

	vec3 N = (inNormal);
	
	vec3 L = normalize(inLightVec);
	
	vec3 V = normalize(inViewVec);
	
	vec3 R = reflect(-L, N);

	vec3 diffuse = max(dot(N, L), 0.0) * infragColor;
	vec3 specular = pow(max(dot(R, V), 0.0), 32.0) * vec3(0.5);

	color = vec4(diffuse * tmpColor.rgb + specular, 1.0);
}