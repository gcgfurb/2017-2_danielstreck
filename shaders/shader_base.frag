#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) in vec3 fragColor;
layout (location = 1) in vec3 inViewVec;
layout (location = 2) in vec3 inLightVec;
layout(location = 3) in vec2 fragTexCoord;
layout (location = 4) in vec3 inNormal;

layout(location = 0) out vec4 outFragColor;

void main() {
       vec4 color = texture(texSampler, fragTexCoord);

	vec3 N = (inNormal);

	vec3 L = normalize(inLightVec);

	vec3 V = normalize(inViewVec);

	vec3 R = reflect(-L, N);

	vec3 diffuse = max(dot(N, L), 0.0) * fragColor;
	vec3 specular = pow(max(dot(R, V), 0.0), 32.0) * vec3(0.5);

	outFragColor = vec4(diffuse * color.rgb + specular, 1.0);
}