#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
    vec4 lightPos;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;
layout(location = 3) in vec3 inNormal;

layout(location = 0) out vec3 fragColor;
layout (location = 1) out vec3 outViewVec;

layout (location = 2) out vec3 outLightVec;
layout(location = 3) out vec2 fragTexCoord;
layout (location = 4) out vec3 outNormal;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition, 1.0);
    fragColor = inColor;
    fragTexCoord = inTexCoord;

	outNormal = normalize(mat3(transpose(inverse(ubo.model))) * inNormal);

	vec4 pos = ubo.model * vec4(inPosition, 1.0);
	vec3 lPos =  ubo.lightPos.xyz;
	outLightVec = lPos - pos.xyz;
	
	outViewVec = pos.xyz;		
}