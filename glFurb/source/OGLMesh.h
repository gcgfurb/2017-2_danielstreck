
//https://learnopengl.com/code_viewer.php?code=model&type=header


#pragma once





// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>

struct OGLVertex {
	glm::vec3 pos;
	glm::vec3 color;
	glm::vec2 texCoord;
	glm::vec3 normal;

	bool operator==(const OGLVertex& other) const {
		return pos == other.pos && color == other.color && texCoord == other.texCoord && normal == other.normal;
	}
};



#include "../source/Mesh.h"
#include "shader.h"
#include "OGLTexture.h"



namespace std {
	template<> struct hash<OGLVertex> {
		size_t operator()(OGLVertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^ (hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^ (hash<glm::vec2>()(vertex.texCoord) << 1);
		}
	};
}



class OGLMesh : public Mesh {
public:
	/*  Mesh Data  */
	vector<OGLVertex> vertices;
	vector<GLuint> indices;

	size_t indicesSize;

	/*  Functions  */
	// Constructor
	OGLMesh(std::string meshPath);


	// Render the mesh
	void Draw(Shader shader, GLuint textureId);
	
private:
	/*  Render data  */
	GLuint VAO, VBO, EBO;

	/*  Functions    */
	// Initializes all the buffer objects/arrays
	void setupMesh();

	void loadModel(std::string MODEL_PATH, std::vector<OGLVertex>& outVertices, std::vector<uint32_t>& outIndices);

};


