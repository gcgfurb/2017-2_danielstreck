#pragma once


#include "../../commonBase/source/Renderer.h"

// glew before everything else gl 
#include <GL\glew.h>

#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#include <GLFW/glfw3.h>

#include <glm\glm.hpp>
#include <glm/gtx/hash.hpp>

#include "OGLMesh.h"
#include "../../commonBase/source/Scene.h"
#include "OGLDrawableObj.h"
#include "OGLMaterial.h"

#include "OGLTexture.h"

#include <vector>

#define PtrDownCast static_cast<OGLDrawableObj*>


class RendererGL : public Renderer
{
public:
	//***SINGLETON****
	static RendererGL* getInstance()
	{
		static RendererGL instance; // Guaranteed to be destroyed.
									// Instantiated on first use.
		return &instance;
	}

	// C++ 11
	// =======
	// We can use the better technique of deleting the methods
	// we don't want.
	RendererGL(RendererGL const&) = delete;
	void operator=(RendererGL const&) = delete;
	//***SINGLETON****
	
	RendererGL();
	~RendererGL();

	void initGLFW();

	// init opengl
	void prepare() override;
	void initOpenGL();
	void drawFrame() override;
	void update(float dt) override;
	void mainLoop() override {};
	
	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{

		RendererGL * renderer = reinterpret_cast<RendererGL*>(glfwGetWindowUserPointer(window));

		if (key == GLFW_KEY_UP && action == GLFW_PRESS)
		{
			renderer->scene->camera->move(glm::vec3(1, 0, 0));
		}

		if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
		{
			renderer->scene->camera->move(glm::vec3(-1, 0, 0));
		}

			
	}


};