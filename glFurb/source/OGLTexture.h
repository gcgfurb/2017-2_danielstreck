#pragma once

#include <string>
#include <gl\glew.h>

class OGLTexture
{
public:

	GLuint id;
	
	OGLTexture() {};

	OGLTexture(std::string texPath)
	{
		id = loadTexture(texPath);
	}
	GLuint loadTexture(std::string texturePath);
};
