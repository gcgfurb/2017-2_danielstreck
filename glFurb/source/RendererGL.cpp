﻿
#include "RendererGL.h"

#include <string>

#include <unordered_map>
#include <vector>
#include <chrono>

#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm\trigonometric.hpp>


#include "Shader.h"
#include "../../commonBase/source/Scene.h"
#include "OGLDrawableObj.h"
#include "OGLMaterial.h"

RendererGL::RendererGL()
{
}

RendererGL::~RendererGL()
{

}

void RendererGL::initGLFW()
{
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // mac

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	_glfwWindow = glfwCreateWindow(WIDTH, HEIGHT, "OpenGL", nullptr, nullptr); // Windowed

	glfwMakeContextCurrent(_glfwWindow);

	glfwSetKeyCallback(_glfwWindow, RendererGL::key_callback);

	glfwSwapInterval(0);

	glfwSetWindowUserPointer(_glfwWindow, this);
}

void RendererGL::prepare()
{
	initGLFW();
	initOpenGL();
	glewExperimental = GL_TRUE;
	glewInit();
}

void RendererGL::initOpenGL()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	//glFrontFace(GL_CCW);

}

void RendererGL::drawFrame()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	for (auto obj : scene->getSceneGraph())
	{
		auto castedObj = static_cast<OGLDrawableObj*>(obj);

		castedObj->material->shader->Use();

		glUniformMatrix4fv(glGetUniformLocation(castedObj->material->shader->Program, "model"), 1, GL_FALSE, glm::value_ptr(castedObj->transform));

		static_cast<OGLMesh*>(castedObj->mesh)->Draw(*castedObj->material->shader, castedObj->material->texture->id);		
	}

	glfwSwapBuffers(_glfwWindow);

}

void RendererGL::update(float dt)
{
	for (auto obj : scene->getSceneGraph())
	{
		obj->update(dt);
	}
}

