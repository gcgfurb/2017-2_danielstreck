#pragma once

#include "Shader.h"
#include "OGLTexture.h"


class OGLMaterial
{
public:

	void setShader(Shader *shader) { this->shader = shader; };

	Shader *shader;
	OGLTexture *texture;
};