#pragma once

#include "../../commonBase/source/DrawableObj.h"

#include "OGLMaterial.h"

class OGLDrawableObj : public DrawableObj
{

public:
	OGLMaterial *material;

	OGLDrawableObj() {};
	~OGLDrawableObj() {};
	
	void update(float dt) override;

};