#include "OGLMesh.h"

#include <unordered_map>


#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>


OGLMesh::OGLMesh(std::string meshPath)
{

	loadModel(meshPath, this->vertices, this->indices);


	this->indicesSize = this->indices.size();

	// Now that we have all the required data, set the vertex buffers and its attribute pointers.
	this->setupMesh();
}

// Render the mesh
void OGLMesh::Draw(Shader shader, GLuint textureId)
{
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE0); 
	glUniform1i(glGetUniformLocation(shader.Program, "texture_difuse1"), 0);

	glBindTexture(GL_TEXTURE_2D, textureId);

	glBindVertexArray(this->VAO);
	// Draw mesh
	glDrawElements(GL_TRIANGLES, this->indicesSize, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void  OGLMesh::setupMesh()
{
	// Create buffers/arrays
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindVertexArray(this->VAO);
	// Load data into vertex buffers
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	// A great thing about structs is that their memory layout is sequential for all its items.
	// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
	// again translates to 3/2 floats which translates to a byte array.
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(OGLVertex), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

	// Set the vertex attribute pointers
	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(OGLVertex), (GLvoid*)0);
	// Vertex Color
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(OGLVertex), (GLvoid*)offsetof(OGLVertex, color));
	// Vertex Texture Coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(OGLVertex), (GLvoid*)offsetof(OGLVertex, texCoord));
	// Vertex Normals
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(OGLVertex), (GLvoid*)offsetof(OGLVertex, normal));

	glBindVertexArray(0);
}

void OGLMesh::loadModel(std::string MODEL_PATH, std::vector<OGLVertex>& outVertices, std::vector<uint32_t>& outIndices)
{
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &err, MODEL_PATH.c_str())) {
		throw std::runtime_error(err);
	}

	std::unordered_map<OGLVertex, uint32_t> uniqueVertices = {};

	for (const auto& shape : shapes) {
		for (const auto& index : shape.mesh.indices) {
			OGLVertex vertex = {};

			vertex.pos =
			{
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};

			if (attrib.normals.size() > 0)
			{
				vertex.normal =
				{
					attrib.normals[3 * index.normal_index + 0], // x
					attrib.normals[3 * index.normal_index + 1], // y
					attrib.normals[3 * index.normal_index + 2]  // z
				};
			}

			// se nao tem info de UV usar pos do vertice para nao crashar
			if (attrib.texcoords.size() == 0)
			{
				vertex.texCoord = vertex.pos;
			}
			else
			{
				vertex.texCoord = {
					attrib.texcoords[2 * index.texcoord_index + 0],
					attrib.texcoords[2 * index.texcoord_index + 1]
				};
			}

			vertex.color = { 1.0f, 1.0f, 1.0f };

			if (uniqueVertices.count(vertex) == 0) {
				uniqueVertices[vertex] = static_cast<uint32_t>(outVertices.size());
				outVertices.push_back(vertex);
			}

			outIndices.push_back(uniqueVertices[vertex]);

		}
	}
}
