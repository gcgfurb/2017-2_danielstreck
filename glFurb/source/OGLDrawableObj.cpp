#include "OGLDrawableObj.h"
#include "OGLMaterial.h"

#include "RendererGL.h"

#include <chrono>


void OGLDrawableObj::update(float dt)
{
	glUniformMatrix4fv(glGetUniformLocation(material->shader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(RendererGL::getInstance()->scene->camera->projection));
	glUniformMatrix4fv(glGetUniformLocation(material->shader->Program, "view"), 1, GL_FALSE, glm::value_ptr(RendererGL::getInstance()->scene->camera->view));
	static auto startTime = std::chrono::high_resolution_clock::now();

	auto currentTime = std::chrono::high_resolution_clock::now();
	float time = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count() / 1000.0f;


	transform = glm::mat4(1.f);
	transform *= glm::translate(glm::mat4(1.f), position);
	transform *= glm::scale(glm::mat4(1.f), scale);
	///transform *= glm::rotate(glm::mat4(1.f), glm::radians(180.0f), glm::vec3(1, 0, 0));
	transform *= glm::rotate(glm::mat4(1.f), time * glm::radians(90.0f), glm::vec3(0, 1.0f, 0.0f));

	glUniform4fv(glGetUniformLocation(material->shader->Program, "lightPos"), 1, glm::value_ptr(RendererGL::getInstance()->scene->light.lightPos));
}
