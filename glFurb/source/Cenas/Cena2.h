#pragma once

#include "../RendererGL.h"

class Cena2 : public Scene
{
public:
	Cena2()
	{
		name = "Cena2";

		Shader *shader = new Shader("../shaders/GL/vert.vert", "../shaders/GL/frag.frag");

		OGLTexture *texture = new OGLTexture("../textures/fur.jpg");


		OGLMaterial *material = new OGLMaterial;
		material->shader = shader;
		material->texture = texture;

		OGLMesh *mesh = new OGLMesh("../models/bunny.obj");

		float row = 150;
		// 289 objs
		for (float i = 0; i < row; i += 3)
		{
			for (float j = 0; j < row; j += 3)
			{

				OGLDrawableObj* obj1(new OGLDrawableObj);
				obj1->mesh = mesh;
				PtrDownCast(obj1)->material = material;
				PtrDownCast(obj1)->material->texture = texture;

				PtrDownCast(obj1)->setPosition(glm::vec3(i - row / 2, 1, j - row / 2));
				PtrDownCast(obj1)->setScale(glm::vec3(1));

				this->AddObject(obj1);
			}
		}

		std::cout << getSceneGraph().size() << std::endl;

		Camera *camera = new Camera;
		camera->view = glm::lookAt(glm::vec3(0, 20, 40.0f), glm::vec3(0.0f, 1.0f, 0), glm::vec3(0.0f, 1.0f, 0.0f));
		camera->projection = glm::perspective(glm::radians(60.0f), (float)RendererGL::getInstance()->WIDTH / RendererGL::getInstance()->HEIGHT, 0.1f, 100.0f);

		Light light;
		light.lightPos = glm::vec4(row / 2, 2, row / 2, 1);

		this->light = light;
		this->camera = camera;
	}
};