#pragma once

#include "../RendererGL.h"

class Cena3 : public Scene
{
public:
	Cena3()
	{
		name = "Cena3";

		OGLTexture *furTexture = new OGLTexture("../textures/fur.jpg");
		OGLTexture *redTexture = new OGLTexture("../textures/red.png");
		OGLTexture *buddhaTexture = new OGLTexture("../textures/teapot.jpg");

		OGLMaterial* material(new OGLMaterial);
		OGLMaterial* redMaterial(new OGLMaterial);
		OGLMaterial* buddhMaterial(new OGLMaterial);

		Shader* shader(new Shader("../shaders/GL/vert.vert", "../shaders/GL/frag.frag"));

		OGLMesh* bunnyMesh(new OGLMesh("../models/bunny.obj"));
		OGLMesh* monkeyMesh(new OGLMesh("../models/monkey.obj"));
		OGLMesh* buddhaMesh(new OGLMesh("../models/buddha.obj"));

		redMaterial->setShader(shader);
		material->setShader(shader);
		buddhMaterial->setShader(shader);

		float row = 10;
		// 120 objs
		for (int j = 0; j < 4; j++)
		{
			for (int i = 0; i < row; i++)
			{
				DrawableObj* obj1(new OGLDrawableObj);

				PtrDownCast(obj1)->material = material;
				PtrDownCast(obj1)->material->texture = (furTexture);

				PtrDownCast(obj1)->mesh = bunnyMesh;

				PtrDownCast(obj1)->setPosition(glm::vec3((i - row / 2) * 3, 1, -(j * 3)));
				PtrDownCast(obj1)->setScale(glm::vec3(1));

				this->AddObject(obj1);
			}

			for (float i = 0; i < row; i++)
			{
				DrawableObj* obj2(new OGLDrawableObj);

				PtrDownCast(obj2)->material = redMaterial;
				PtrDownCast(obj2)->material->texture = (redTexture);

				PtrDownCast(obj2)->mesh = (monkeyMesh);

				PtrDownCast(obj2)->setPosition(glm::vec3((i - row / 2) * 3, 1, -((j + 3) * 6)));
				PtrDownCast(obj2)->setScale(glm::vec3(1));

				this->AddObject(obj2);
			}

			for (float i = 0; i < row; i++)
			{
				DrawableObj* obj3(new OGLDrawableObj);

				PtrDownCast(obj3)->material = buddhMaterial;
				PtrDownCast(obj3)->material->texture = (buddhaTexture);


				PtrDownCast(obj3)->mesh = (buddhaMesh);

				PtrDownCast(obj3)->setPosition(glm::vec3((i - row / 2) * 3, 1, 2));
				PtrDownCast(obj3)->setScale(glm::vec3(2.5));

				this->AddObject(obj3);
			}
		}


		std::cout << getSceneGraph().size() << std::endl;

		Camera *camera = new Camera;
		camera->view = glm::lookAt(glm::vec3(-1, 8, 12.0f), glm::vec3(-1.0f, 1.0f, -4), glm::vec3(0.0f, 1.0f, 0.0f));
		camera->projection = glm::perspective(glm::radians(60.0f), (float)RendererGL::getInstance()->WIDTH / RendererGL::getInstance()->HEIGHT, 0.1f, 100.0f);

		Light light;
		light.lightPos = glm::vec4(row / 2, 2, row / 2, 1);

		this->light = light;
		this->camera = camera;
	}
};