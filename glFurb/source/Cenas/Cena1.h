#pragma once

#include "../RendererGL.h"

class Cena1 : public Scene
{
public:
	Cena1()
	{
		name = "Cena1";
		
		Shader *shader = new Shader("../shaders/GL/vert.vert", "../shaders/GL/frag.frag");

		OGLTexture *texture = new OGLTexture("../textures/bronze.jpg");

		OGLMaterial *material = new OGLMaterial;
		material->shader = shader;
		material->texture = texture;

		OGLDrawableObj* obj1(new OGLDrawableObj);
		{
			PtrDownCast(obj1)->mesh = new OGLMesh("../models/buddha.obj");
			PtrDownCast(obj1)->material = material;
			PtrDownCast(obj1)->material->texture = texture;
			PtrDownCast(obj1)->setPosition(glm::vec3(0));
			PtrDownCast(obj1)->setScale(glm::vec3(1));
		}

		this->AddObject(obj1);

		Camera *camera = new Camera;
		camera->view = glm::lookAt(glm::vec3(0, 0.5f, 1), glm::vec3(0.0f, 0.1f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		camera->projection = glm::perspective(glm::radians(60.0f), (float)RendererGL::getInstance()->WIDTH / RendererGL::getInstance()->HEIGHT, 0.1f, 100.0f);

		Light light;
		light.lightPos = glm::vec4(0, 5, 10, 0);

		this->light = light;
		this->camera = camera;
	}
};